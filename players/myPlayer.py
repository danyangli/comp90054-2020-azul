from advance_model import *
from utils import *
# if you want to use random, you have to import it here
import random

# if you want to import other py file in the same directory as your myPlayer.py
import sys
sys.path.append("players/Magneto/")

class myPlayer(AdvancePlayer):
    def __init__(self, _id):
        super().__init__(_id)

    def SelectMove(self, moves, game_state):
        best_move = None
        highest_score = -100
        res = self.calculate_h(moves, game_state)
        for mid, fid, tgrab, h in res:
            if h > highest_score:
                highest_score = h
                best_move = (mid, fid, tgrab)
        return best_move

    def calculate_h(self, moves, game_state):
        ps = game_state.players[self.id]
        res = []
        for mid, fid, tgrab in moves:
            h = 0
            i = tgrab.pattern_line_dest
            if i == -1:
                h=-50
            if tgrab.num_to_pattern_line >= i+1:
                h+=10  # 填满加分
                #print("可以填满")
                # 可填满时，考虑grid_state摆放情况
                grid_col = int(ps.grid_scheme[i][tgrab.tile_type])
                if grid_col == 2 and i == 2:
                    h += 50
                    #print("转移到中心点", h)
                elif i >= 1 and i <= 3 and grid_col >= 1 and grid_col <= 3:  # 填满后，放第中心区域加分
                    h += 20
                    #print("转移到中央区", h)

                right = grid_col + 1
                while right in range(0, ps.GRID_SIZE):
                    if ps.grid_state[i][right] == 1:
                        h += 10
                        right += 1
                    else:
                        #print("break right")
                        break

                left = grid_col - 1
                while left in range(0, ps.GRID_SIZE):
                    if ps.grid_state[i][left] == 1:
                        h += 10
                        left -= 1
                    else:
                        #print("break left")
                        break

                up = i + 1
                while up in range(0, ps.GRID_SIZE):
                    if ps.grid_state[up][grid_col] == 1:
                        h += 10
                        up += 1
                    else:
                        #print("break up")
                        break

                down = i - 1
                while down in range(0, ps.GRID_SIZE):
                    if ps.grid_state[down][grid_col] == 1:
                        h += 10
                        down -= 1
                    else:
                        #print("break down")
                        break
            if tgrab.num_to_floor_line > 0:  # 多余扣分
                h = h - (4 * tgrab.num_to_floor_line)  # 按地板行个数平均
            res.append((mid,fid,tgrab,h))
        return res





