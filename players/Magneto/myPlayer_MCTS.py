# This is copied from Naive_player to test if we established a working agent.
# python runner.py -r naive_player -b myPlayer
# starts a fully automated game where the red team is a NaivePlayer and blue team is myPlayer
# This is copied from Naive_player to test if we established a working agent.
# python runner.py -r naive_player -b myPlayer
# starts a fully automated game where the red team is a NaivePlayer and blue team is myPlayer
from typing import List, Any, Tuple

from advance_model import *
from players import naive_player
from utils import *
from model import *
from abc import ABC, abstractmethod
from collections import defaultdict
import math
import numpy as np
import copy
import sys
import math
import random


class myPlayer(AdvancePlayer):
    def __init__(self, _id):
        super().__init__(_id)

    def SelectMove(self, moves, game_state):
        best_move = None
        # player_state = game_state.players[self.0]
        cp_state = copy.deepcopy(game_state)
        init_state = State(cp_state, self.id)
        init_node = Node()
        init_node.set_state(init_state)
        current_node = init_node
        current_node = monte_carlo_tree_search(current_node)
        best_move = current_node.state.get_action()
        return best_move


class State(object):

    def __init__(self, state, id):
        self.id = id
        self.state = copy.deepcopy(state)
        self.action = None

    def get_action(self):
        return self.action

    def set_action(self, action):
        self.action = action

    def is_terminal(self):

        return not(self.state.TilesRemaining())  # players[self.id].GetCompletedRows()

    def compute_reward(self):
        reward = self.state.players[self.id].ScoreRound()[0]
        return reward

    def get_next_state_with_random_choice(self):
        random_choice = random.choice(self.state.players[self.id].GetAvailableMoves(self.state))
        self.state.ExecuteMove(0, random_choice)
        next_state = State(self.state, self.id)
        next_state.set_action(random_choice)

        return next_state


# Tree structure
class Node(object):

    def __init__(self):

        self.parent = None
        self.children = []
        self.visit_times = 0
        self.quality_value = 0.0
        self.state = None

    def set_state(self, state):
        self.state = state

    def get_state(self):
        return self.state

    def set_parent(self, parent):
        self.parent = parent

    def get_parent(self):
        return self.parent

    def get_children(self):
        return self.children

    def set_visit_times(self, times):
        self.visit_times = times

    def get_visit_times(self):
        return self.visit_times

    def visit_times_add_one(self):
        self.visit_times += 1

    def set_quality_value(self, value):
        self.quality_value = value

    def get_quality_value(self):
        return self.quality_value

    def quality_value_add_n(self, n):
        self.quality_value += n

    def is_all_expand(self):
        return len(self.children) == 20 #len(self.state.players[self.id].GetAvailableMoves(self.state))

    def add_child(self, sub_node):
        sub_node.set_parent(self)
        self.children.append(sub_node)


def tree_policy(node):
    # select the node to expand
    while not node.get_state().is_terminal():

        if node.is_all_expand():
            node = best_child(node, True)
        else:
            sub_node = expand(node)
            return sub_node

    return node


def expand(node):

    tried_sub_node_states = [sub_node.get_state() for sub_node in node.get_children()]
    new_state = node.get_state().get_next_state_with_random_choice()

    # check if the new state is different with others
    while new_state in tried_sub_node_states:
        new_state = node.get_state().get_next_state_with_random_choice()

    sub_node = Node()
    sub_node.set_state(new_state)
    node.add_child(sub_node)

    return sub_node


def best_child(node, is_exploration):
    # find the child node with the highest score by UCB
    best_score = -sys.maxsize
    best_sub_node = None

    for sub_node in node.get_children():

        if is_exploration:
            C = 1 / math.sqrt(2.0)
        else:
            C = 0.0

        left = sub_node.get_quality_value() / sub_node.get_visit_times()
        right = 2.0 * math.log(node.get_visit_times()) / sub_node.get_visit_times()
        score = left + C * math.sqrt(right)

        if score > best_score:
            best_sub_node = sub_node
            best_score = score

    return best_sub_node


def default_policy(node):
    # Simulation phase of MCTS, input a node that need to expand,
    # then create a node with a random move, and return the
    # reward of the new node
    current_state = node.get_state()

    while not current_state.is_terminal():
        current_state = current_state.get_next_state_with_random_choice()

    final_state_reward = current_state.compute_reward()
    return final_state_reward


def backup(node, reward):
    # backpropagation phase, iteratively update all the parent nodes of the sub node
    while node is not None:
        # update the visit times
        node.visit_times_add_one()
        # update the quality value
        node.quality_value_add_n(reward)
        node = node.parent


def monte_carlo_tree_search(node):
    # input a root node, return the best child node
    computation_budget = 2
    for i in range(computation_budget):
        # find the best node to expand
        expand_node = tree_policy(node)
        # expand the node with random move
        reward = default_policy(expand_node)
        # update the nodes with reward
        backup(expand_node, reward)

    best_next_node = best_child(node, False)

    return best_next_node

# class MCTS:
#
#     def __init__(self, exploration_weight=1):
#         self.Q = defaultdict(int)
#         self.N = defaultdict(int)
#         self.children = dict()
#         self.exploration_weight = exploration_weight
#
#     def choose(self, node):
#         if node.get_state().TilesRemaining():
#             raise RuntimeError(f"choose called on terminal node{node}")
#         if node not in self.children:
#             return node.find_random_child()
#
#         def score(n):
#             if self.N[n] == 0:
#                 return float("-inf")
#             return self.Q[n] / self.N[n]
#         return max(self.children[node], key=score)
#
#     def do_rollout(self, node):
#         path = self._select(node)
#         leaf = path[-1]
#         self._expend(leaf)
#         reward = self.simulate(leaf)
#         self._backpropagate(path, reward)
#
#     def _select(self, node):
#         path = []
#         while True:
#             path.append(node)
#             if node not in self.children or not self.children[node]:
#                 return path
#             unexplored = self.children[node] - self.children.keys()
#             if unexplored:
#                 n = unexplored.pop()
#                 path.append(n)
#                 return path
#             node = self._uct_select(node)
#
#     def _expand(self, node):
#         if node in self.children:
#             return
#         self.children[node] = node.find_children()
#
#     def _simulate(self, node):
#         invert_reward = True
#         while True:
#             if node.get_state().TilesRemaining():
#                 reward = node.reward()
#                 return 1 - reward if invert_reward else reward
#             node = node.find_random_child()
#             invert_reward = not invert_reward
#
#     def _backpropagate(self, path, reward):
#         for node in reversed(path):
#             self.N[node] += 1
#             self.Q[node] += reward
#             reward = 1 - reward
#
#     def _uct_select(self, node):
#         assert all(n in self.children for n in self.children[node])
#
#         log_N_vertex =  math.log(self.N[node])
#
#         def uct(n):
#             return self.Q[n] / self.N[n] + self.exploration_weight * math.sqrt(log_N_vertex/self.N[n])
#
#         return max(self.children[node], key=uct)
#
# class Node(ABC):
#
#     def find_children(self):
#         return set()
#
#     def find_random_child(self):
#         return None
#
#     def is_terminal(self):
#         return True
#
#     def reward(self):
#         return 0
#
#     def __hash__(self):
#         return 123456789
#
#     def __eq__(node1, node2):
#         return True
#



# class TreeNode(object):
#     def __init__(self, parent, prior_p):
#         self._parent = parent
#         self._children = {}
#         self._n_visits = 0
#         self._Q = 0
#         self._u = 0
#         self._P = prior_p
#
#     def select(self, c_puct):
#         return max(self._children.items(), key=lambda act_node: act_node[1].get_value(c_puct))
#
#     def get_value(self, c_puct):
#         self._u = c_puct * self._P * np.sqrt(self._parent._n_visits) / (1+self._n_visits)
#         return self._Q + self._u
#
#     def expand(self, action_priors):
#         for action, prob in action_priors:
#             if action not in self._children:
#                 self._children[action] = TreeNode(self, prob)
#
#     def update(self, leaf_value):
#         self._n_visits += 1
#         self._Q += 1.0 * (leaf_value - self._Q) / self._n_visits
#
#     def update_recursive(self, leaf_value):
#         if self._parent:
#             self._parent.update_recursive(-leaf_value)
#         self.update(leaf_value)
#
#     def is_leaf(self):
#         return self._children == {}
#
#     def is_root(self):
#         return self._parent is None
#
#
# class MCTS(object):
#     def __init__(self, policy_value_fn, c_puct=5, n_playout=10000):
#         self._root = TreeNode(None, 1.0)
#         self._policy = policy_value_fn
#         self._c_puct = c_puct
#         self._n_playout = n_playout
#
#     def _playout(self, state):
#         node = self._root
#         while True:
#             if node.is_leaf():
#                 break
#             action, node = node.select(self._c_puct)
#             state.do_move(action)
#
#             action_probs, leaf_value = self._policy(state)
#
#             end, winner = state.game_end()
#             if not end:
#                 node.expand(action_probs)
#             else:
#                 if winner == -1:
#                     leaf_value = 0.0
#                 else:
#                     leaf_value = (1.0 if winner == state.get_current_player() else -1.0
#                                   )
#             node.update_recursive(-leaf_value)
















#
# class myPlayer(AdvancePlayer):
#     def __init__(self, _0):
#         super().__init__(_0)
#
#     def SelectMove(self, moves, game_state):
#         best_move = None
#         highest_score = 0
#         res = self.calculate_h(moves, game_state)
#         for m0, f0, tgrab, h in res:
#             if h > highest_score:
#                 highest_score = h
#                 best_move = (m0, f0, tgrab)
#                 # print("跑到这里了吗？", best_move)
#         return best_move
#
#     def calculate_h(self, moves, game_state):
#         ps = game_state.players[self.0]
#         res = []
#         for m0, f0, tgrab in moves:
#             h = 0
#             i = tgrab.pattern_line_dest
#             if tgrab.num_to_pattern_line >= i+1:
#                 h+=10  # 填满加分
#                 print("可以填满")
#                 # 可填满时，考虑gr0_state摆放情况
#                 gr0_col = int(ps.gr0_scheme[i][tgrab.tile_type])
#                 if gr0_col == 2 and i == 2:
#                     h += 50
#                     print("转移到中心点", h)
#                 elif i >= 1 and i <= 3 and gr0_col >= 1 and gr0_col <= 3:  # 填满后，放第中心区域加分
#                     h += 20
#                     print("转移到中央区", h)
#
#                 right = gr0_col + 1
#                 while right in range(0, ps.GR0_SIZE):
#                     if ps.gr0_state[i][right] == 1:
#                         h += 10
#                         right += 1
#                     else:
#                         print("break right")
#                         break
#
#                 left = gr0_col - 1
#                 while left in range(0, ps.GR0_SIZE):
#                     if ps.gr0_state[i][left] == 1:
#                         h += 10
#                         left -= 1
#                     else:
#                         print("break left")
#                         break
#
#                 up = i + 1
#                 while up in range(0, ps.GR0_SIZE):
#                     if ps.gr0_state[up][gr0_col] == 1:
#                         h += 10
#                         up += 1
#                     else:
#                         print("break up")
#                         break
#
#                 down = i - 1
#                 while down in range(0, ps.GR0_SIZE):
#                     if ps.gr0_state[down][gr0_col] == 1:
#                         h += 10
#                         down -= 1
#                     else:
#                         print("break down")
#                         break
#             if tgrab.num_to_floor_line >= 0:  # 多余扣分
#                 h -= 3 * tgrab.num_to_floor_line  # 按地板行个数平均
#             res.append((m0,f0,tgrab,h))
#         return res
#
#


# from advance_model import *
# from utils import *
#
#
# class myPlayer(AdvancePlayer):
#     def __init__(self, _0):
#         super().__init__(_0)
#
#
#

#    def SelectMove(self, moves, game_state):
#
#         best_move = None
#         this_player = game_state.players[0]
#         myQueue = []
#
#         for i in range(5):
#             for m0, f0, tgrab in moves:
#                 if this_player.lines_tile == -1:  # empty line
#                     if tgrab.number == i+1:  # fit the line
#                         myQueue.insert(0, (m0, f0, tgrab))
#                         break
#                 else:  #have tile
#                     if this_player.lines_number[i] + tgrab.number == i+1 and \
#                             this_player.lines_tile[i] == tgrab.tile_type:  # fit the line
#                         myQueue.insert(0, (m0, f0, tgrab))
#                         break
#         for i in range(5):
#             for m0, f0, tgrab in moves:
#                 if this_player.lines_tile == -1:  # empty line
#                     if (i+1) - this_player.lines_number[i] == 1:  # lack of one
#                         myQueue.insert(0, (m0, f0, tgrab))
#                         break
#                 else:
#                     if (i+1) - this_player.lines_number[i] == 1 and \
#                             this_player.lines_tile[i] == tgrab.tile_type:
#                         myQueue.insert(0, (m0, f0, tgrab))
#                         break
#
#         for i in range(5):
#             for m0, f0, tgrab in moves:
#                 if this_player.lines_tile == -1:
#                     if (i+1) - this_player.lines_number[i] == 2:
#                         myQueue.insert(0, (m0, f0, tgrab))
#                         break;
#                 else:
#                     if (i+1) - this_player.lines_number[i] == 2 and \
#                             this_player.lines_tile[i] == tgrab.tile_type:
#                         myQueue.insert(0, (m0, f0, tgrab))
#                         break
#
#         for m0, f0, tgrab in moves:
#             if tgrab.num_to_floor_line < 3:
#                 myQueue.insert(0, (m0, f0, tgrab))
#                 break
#         if len(myQueue) == 0:
#             best_move = moves[0]
#         else:
#             best_move = myQueue.pop()
#         return best_move



    # def SelectMove(self, moves, game_state):
        # Select move that involves placing the most number of tiles
        # in a pattern line. Tie break on number placed in floor line.
        # most_to_line = -1
        # corr_to_floor = 0
        # centre_num = game_state.centre_pool.total
        #
        # best_move = moves[-1]

        # for m0, f0, tgrab in moves:
        #     if m0 == Move.TAKE_FROM_CENTRE and centre_num != 0:
        #         if most_to_line == -1:
        #             best_move = (m0, f0, tgrab)
        #             most_to_line = tgrab.number
        #             corr_to_floor = tgrab.num_to_floor_line
        #             continue
        #
        #         if tgrab.number > most_to_line:
        #             best_move = (m0, f0, tgrab)
        #             most_to_line = tgrab.number
        #             corr_to_floor = tgrab.num_to_floor_line
        #         elif tgrab.number == most_to_line and \
        #                 tgrab.number < corr_to_floor:
        #             best_move = (m0, f0, tgrab)
        #             most_to_line = tgrab.number
        #             corr_to_floor = tgrab.num_to_floor_line
        #
        #     else:
        #         best_move = moves[0]
        # return best_move
#
# class myPlayer(AdvancePlayer):
#     def __init__(self, _0):
#         super().__init__(_0)
#
#     def SelectMove(self, moves, game_state):
#         best_move = None
#         heuristic = 100000
#         res = self.calculate_h(moves, game_state)
#         for m0, f0 , tgrab, h in res:
#             if h < heuristic:
#                 heuristic = h
#                 best_move = (m0, f0, tgrab)
#         return best_move
#
#     def calculate_h(self, moves, game_state):
#         ps = game_state.players[0]
#         res = []
#         for m0, f0, tgrab in moves:
#             h = 0
#             i = tgrab.pattern_line_dest
#             if i == -1:
#                 h += 50
#             if tgrab.num_to_pattern_line + ps.lines_number[i] < i:
#                 h += 20
#             if 3 > tgrab.num_to_floor_line and tgrab.num_to_floor_line > 0:
#                 h += 20
#             if 2 < tgrab.num_to_floor_line and tgrab.num_to_floor_line < 5:
#                 h += 50
#         res.append((m0,f0,tgrab,h))
#         return res
