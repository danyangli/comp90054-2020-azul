# This is copied from Naive_player to test if we established a working agent.
# python runner.py -r naive_player -b myPlayer
# starts a fully automated game where the red team is a NaivePlayer and blue team is myPlayer

from advance_model import *
from utils import *


class myPlayer(AdvancePlayer):
    def __init__(self, _id):
        super().__init__(_id)

    def SelectMove(self, moves, game_state):
        # Select move that involves placing the most number of tiles
        # in a pattern line. Tie break on number placed in floor line.
        most_to_line = -1
        corr_to_floor = 0
        player = None
        best_move = None
        targetPatternLine = 0
        moves_for_other_patternline = []
        for pl in game_state.players:
            if pl.id == self.id:
                player = pl

        # Find a pattern line which has not been full-filled from up to down, if none is found,
        # return a move that has least tiles to the floor
        while player.lines_number[targetPatternLine] == targetPatternLine+1:
            targetPatternLine += 1
            if targetPatternLine == len(player.lines_number):
                targetPatternLine = -1
                return self.best_move_to_floor(moves, targetPatternLine)

        # Find a best move for the targetPatternLine
        for mid, fid, tgrab in moves:
            if tgrab.pattern_line_dest != targetPatternLine:
                moves_for_other_patternline.append((mid, fid, tgrab))
                continue

            if most_to_line == -1:
                best_move = (mid, fid, tgrab)
                most_to_line = tgrab.num_to_pattern_line
                corr_to_floor = tgrab.num_to_floor_line
                continue

            if tgrab.num_to_pattern_line > most_to_line:
                best_move = (mid, fid, tgrab)
                most_to_line = tgrab.num_to_pattern_line
                corr_to_floor = tgrab.num_to_floor_line
            elif tgrab.num_to_pattern_line == most_to_line and tgrab.num_to_floor_line < corr_to_floor:
                best_move = (mid, fid, tgrab)
                most_to_line = tgrab.num_to_pattern_line
                corr_to_floor = tgrab.num_to_floor_line

        # Find a move that has highest effort
        most_to_line = -1
        corr_to_floor = 0
        if best_move == None:
            for mid, fid, tgrab in moves_for_other_patternline:
                if most_to_line == -1:
                    best_move = (mid, fid, tgrab)
                    most_to_line = tgrab.num_to_pattern_line
                    corr_to_floor = tgrab.num_to_floor_line
                    continue
                if corr_to_floor >= tgrab.num_to_floor_line and most_to_line <= tgrab.num_to_pattern_line:
                    best_move = (mid, fid, tgrab)
                    most_to_line = tgrab.num_to_pattern_line
                    corr_to_floor = tgrab.num_to_floor_line

        return best_move

    def best_move_to_floor(self, moves, targetPatternLine):
        least=7
        result=None
        for mid, fid, tgrab in moves:
            if tgrab.pattern_line_dest == targetPatternLine:
                if tgrab.num_to_floor_line < least:
                    least = tgrab.num_to_floor_line
                    result = (mid, fid, tgrab)
        return result